<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \danog\MadelineProto\API as Telegram;
use Session;
use File;

class UserTelegramController extends Controller
{

    public function __construct()
    {
        //
    }

    public function sendCode(Request $request)
    {
        $settings = [
            'authorization' => [
                'default_temp_auth_key_expires_in' => 315576000, // я установил 10 лет, что бы не авторизовывать приложение повторно.
            ],
            'app_info' => [ // Эти данные мы получили после регистрации приложения на https://my.telegram.org
                'api_id'          => '257319',
                'api_hash'        => '0fd5710b6367c6f88be32fe68fc71951'
            ],
            'logger' => [ // Вывод сообщений и ошибок
                'logger' => 3, // выводим сообещения через echo
                'logger_level' => 'FATAL ERROR', // выводим только критические ошибки.
            ],
            'max_tries' => [ // Количество попыток установить соединения на различных этапах работы. Лучше не уменьшать, так как телеграм не всегда отвечает с первого раза
                'query' => 5, 
                'authorization' => 5,
                'response' => 5,
            ],
            'updates' => [ // Я обновляю данные прямыми запросами, поэтому обновления с каналов и чатов мне не требуются.
                'handle_updates' => false, 
                'handle_old_updates' => false,
            ],
        ];

        $userTelegram = new Telegram($settings);

        $userTelegram->phone_login($request->phone);

        $userTelegram->session = 'login.session.madeline';
        $userTelegram->serialize();

        return response()->json('asd');
    }

    public function login(Request $request)
    {
        $userTelegram = new Telegram('login.session.madeline');
        $authorization = $userTelegram->complete_phone_login($request->code);
        $userTelegram->session = 'session.madeline';
        $userTelegram->serialize();

        File::delete('login.session.madeline', 'login.session.madeline.lock');
    }

    public function loginStatus()
    {
        if(file_exists('session.madeline'))
        {
            $status = true;
        }
        else
        {
            $status = false;
        }

        return response()->json(['status' => $status]);
    }

    public function logout()
    {
        File::delete('session.madeline', 'session.madeline.lock');

        return response()->json(['status' => true]);
    }
    
}