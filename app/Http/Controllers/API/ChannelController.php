<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Channel;
use App\Http\Resources\Channel as ChannelResource;
use App\Http\Controllers\API\TelegramApiController;

class ChannelController extends Controller
{
    private $channel;
    private $telegram;

    public function __construct(Channel $channel, TelegramApiController $telegram)
    {
        $this->channel = $channel;
        $this->telegram = $telegram;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channels = Channel::all();

        return ChannelResource::collection($channels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $searchResult = $this->telegram->search($request);
        
        if(is_array($searchResult) && array_key_exists('success', $searchResult) && $searchResult['success'] == false)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Channel does not exist.'
            ]); 
        }
        else
        {
            $channel = json_decode($searchResult);

            if(isset($channel->Chat))
            {
                try
                {
                    $this->channel->t_id = $channel->Chat->id;
                    $this->channel->name = $channel->Chat->title;
                    $this->channel->username = $request->username;
                    $this->channel->category_id = $request->category_id;
                    $this->channel->participants = (int) $channel->full->participants_count;
                    $this->channel->description = $channel->full->about;
                    $this->channel->save();
                }
                catch(\Illuminate\Database\QueryException $e)
                {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Channel already added.'
                    ]); 
                }

                return response()->json([
                    'status' => 'success',
                    'message' => 'Channel added.'
                ]); 
            }
            else
            {
                return response()->json([
                    'status' => 'error',
                    'message' => 'It is not channel.'
                ]); 
            }
        }
    }

    public function getByCategory($categoryId)
    {
        $channels = $this->channel->all()->where('category_id', $categoryId);

        return ChannelResource::collection($channels);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
