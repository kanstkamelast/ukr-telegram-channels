<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \danog\MadelineProto\API as Telegram;
use Session;

class TelegramApiLoginController extends Controller
{
    public function index()
    {
        // Если файл с сессией уже существует, использовать его
        if(file_exists( 'session.madeline' ) ) {
            $madeline = new Telegram( 'session.madeline' );
        } else {
        // Иначе создать новую сессию
            $madeline = [
                'authorization' => [
                    'default_temp_auth_key_expires_in' => 315576000, // я установил 10 лет, что бы не авторизовывать приложение повторно.
                ],
                'app_info' => [ // Эти данные мы получили после регистрации приложения на https://my.telegram.org
                    'api_id'          => '257319',
                    'api_hash'        => '0fd5710b6367c6f88be32fe68fc71951'
                ],
                'logger' => [ // Вывод сообщений и ошибок
                    'logger' => 3, // выводим сообещения через echo
                    'logger_level' => 'FATAL ERROR', // выводим только критические ошибки.
                ],
                'max_tries' => [ // Количество попыток установить соединения на различных этапах работы. Лучше не уменьшать, так как телеграм не всегда отвечает с первого раза
                    'query' => 5, 
                    'authorization' => 5,
                    'response' => 5,
                ],
                'updates' => [ // Я обновляю данные прямыми запросами, поэтому обновления с каналов и чатов мне не требуются.
                    'handle_updates' => false, 
                    'handle_old_updates' => false,
                ],
            ];
            $MadelineProto = new Telegram($madeline);
            
            $MadelineProto->phone_login(readline('Enter your phone number: ')); //вводим в консоли свой номер телефона
            $authorization = $MadelineProto->complete_phone_login(readline('Enter the code you received: ')); // вводим в консоли код авторизации, который придет в телеграм
            if ($authorization['_'] === 'account.noPassword') {
                throw new \danog\MadelineProto\Exception('2FA is enabled but no password is set!');
            }
            if ($authorization['_'] === 'account.password') {
                $authorization = $MadelineProto->complete_2fa_login(readline('Please enter your password (hint '.$authorization['hint'].'): ')); //если включена двухфакторная авторизация, то вводим в консоли пароль.
            }
            if ($authorization['_'] === 'account.needSignup') {
                $authorization = $MadelineProto->complete_signup(readline('Please enter your first name: '), readline('Please enter your last name (can be empty): '));
            }

            $MadelineProto->session = 'session.madeline';
            $MadelineProto->serialize(); // Сохраняем настройки сессии в файл, что бы использовать их для быстрого подключения. 
        }

        print('LOGGED IN');
    }
}
