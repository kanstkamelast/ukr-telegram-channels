<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \danog\MadelineProto\API as Telegram;
use Session;
use File;

class TelegramApiController extends Controller
{

    private $MadelineProto;

    public function __construct()
    {
        if(file_exists('session.madeline'))
        {
            $this->MadelineProto = new Telegram('session.madeline');
        }
    }

    public function search(Request $request)
    {
        $search = str_replace("@","", $request->input('username'));
        
        if(is_null($this->MadelineProto))
        {
            return response()->json(['error' => 'You have to login', 'success' => false]);
        }

        try
        {
            $type = null;
            $id = null;
            $photo = null;
            $data = $this->MadelineProto->get_full_info('@' . $search);
            $data['success'] =  true;

            if (array_key_exists("User",$data))
            {
                $type = 'user';
                $id = $data['User']['id'];
                $photo = $data['full']['profile_photo'];
            } 
            elseif (array_key_exists("Chat",$data))
            {
                $type = 'chat';
                $id = $data['Chat']['id'];
                $photo = $data['full']['chat_photo'];
            }
            
            $path = public_path("t_images/telegram_personal_photos/$id");

            if(!File::exists($path))
            {
                File::makeDirectory($path);
            }

            $messageMediaPhoto = ['_' => 'messageMediaPhoto', 'photo' => $photo, 'caption' => '' ];
            $photoData = $this->MadelineProto->download_to_dir($messageMediaPhoto, $path);

            if(is_dir($path))
            {
                $file = scandir($path, 1);
                if (is_file($path.'/'.$file[0]))
                    $data['image'] =  "t_images/telegram_personal_photos/$id/$file[0]";
                else $data['image'] = NULL;
            }
            else $data['image'] = NULL;

            $data['type'] = $type;

            return json_encode($data);
        }
        
        catch(\danog\MadelineProto\Exception $exception)
        {
            return response()->json(['error' => 'Not found', 'success' => false]);
        }

        catch(\danog\MadelineProto\RPCErrorException $exception)
        {
            return response()->json(['error' => 'Not found', 'success' => false]);
        }
    }

}
