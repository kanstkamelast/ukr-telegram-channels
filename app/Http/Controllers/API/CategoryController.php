<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Http\Resources\Category as CategoryResource;

class CategoryController extends Controller
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getCategories()
    {
        $categories = $this->category->all();

        return CategoryResource::collection($categories);
    }

    public function getAllActiveCategories()
    {
        $categories = $this->category->where('status', 1)->get();

        return CategoryResource::collection($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slug = self::slugify($request->name);

        if(is_null($slug))
            return response()->json([
                'status' => 'error',
                'massage' => 'Something went wrong.'
            ]);
        else
        {
            $this->category->name = $request->name;
            $this->category->slug = $slug;
            $this->category->status = $request->status;
            $this->category->save();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $slug = self::slugify($request->name);

        if(is_null($slug))
            return response()->json([
                'status' => 'error',
                'massage' => 'Something went wrong.'
            ]);
        else
        {
            $cat = $this->category->find($request->id);
            $cat->name = $request->name;
            $cat->slug = $slug;
            $cat->status = $request->status;
            $cat->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $cat = $this->category->find($category)->first();
        $cat->delete();

        return response()->json([
            'status' => 'success',
            'massage' => 'Category deleted.'
        ]);
    }

    static public function slugify($text)
    {
        $text = transliterator_transliterate("Any-Latin; NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();", $text);
        $text = preg_replace('/[-\s]+/', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);

        if (empty($text)) {
            return null;
        }

        return $text;
    }

    public function getCategoryBySlug($categorySlug)
    {
        $category = $this->category->where('status', 1)->where('slug', $categorySlug)->with('channels')->get();

        if($category->count() == 0)
        {
            return response()->json([
                'status' => 'error',
                'massage' => 'Category not found.'
            ]);
        }
        else return new CategoryResource($category->first());
        
    }

}
