<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Channel extends Model
{
    protected $fillable = ['name', 'username', 'category_id', 't_id', 'participants', 'description'];


    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function photo()
    {
        $path = public_path("t_images/telegram_personal_photos/$this->t_id");
        if(is_dir($path))
        {
            $file = scandir($path, 1);
            if (is_file($path.'/'.$file[0]))
                return  "t_images/telegram_personal_photos/$this->t_id/$file[0]";
            else return NULL;
        }
        else return NULL;
    }
}
