<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Channel;

class Category extends Model
{
    public function channels()
    {
        return $this->hasMany(Channel::class);
    }
}
