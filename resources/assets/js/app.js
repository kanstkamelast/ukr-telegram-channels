
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Element from 'element-ui'

import Home from './components/HomeComponent'
import Search from './components/SearchComponent'
import Chanels from './components/ChanelsComponent'
import Categories from './components/CategoriesComponent'
import CategoryChannels from './components/CategoryChannelsComponent'
import AddChanel from './components/AddChanelComponent'
import Admin from './admin/BaseComponent'
import Login from './user/LoginComponent'

Vue.component('havbar', require('./components/NavbarComponent'));
Vue.component('footer', require('./components/FooterComponent'));

Vue.component('admin-categories', require('./admin/components/CategoryComponent'));
Vue.component('admin-channels', require('./admin/components/ChannelComponent'));


Vue.use(Element)
Vue.use(VueRouter);
Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]').content;

const router = new VueRouter({
  mode: 'history',
  routes: [
    /**
     * Admin routes
     */
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
    },
    /**
     * main app routes
     */
    {
      path: '/search',
      name: 'search',
      component: Search,
    },
    {
      path: '/categories',
      name: 'categories',
      component: Categories,
    },
    {
      path: '/categories/:categorySlug',
      name: 'category.index',
      component: CategoryChannels,
    },
    {
      path: '/chanels',
      name: 'chanels',
      component: Chanels,
    },
    {
      path: '/chanels/add',
      name: 'add_chanel',
      component: AddChanel,
    },
    /**
     * Login
     */
    {
      path: '/user/login',
      name: 'user.login',
      component: Login,
    },
  ],
});


const app = new Vue({
    el: '#app',
    render: h => h(Home),
    router,
  });