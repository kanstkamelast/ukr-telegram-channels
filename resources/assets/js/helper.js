export default class Helpers {
    static loadingMaterializeCSS() {
        $(document).ready(() => {
            Materialize.updateTextFields();
 
            let select = $('select');
            if (typeof select.material_select !== 'undefined') {
                select.material_select('destroy');
            }
            select.material_select();
 
            $('.character-counter').characterCounter();
 
            $('.tooltipped').tooltip({
                delay: 50,
                position: 'top'
            });
        });
    }
}