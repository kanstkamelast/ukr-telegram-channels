<?php

use Illuminate\Http\Request;
use App\Http\Controllers\API\TelegramApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


/**
 * USE ONLY ONCE
 */
Route::get('login', 'API\TelegramApiLoginController@index');


Route::get('channels', 'API\ChannelController@index');


Route::get('test', 'API\TelegramApiController@index');
Route::post('search', 'API\TelegramApiController@search');

Route::get('categories', 'API\CategoryController@getAllActiveCategories');
Route::get('categories/getCategoryBySlug/{categorySlug}', 'API\CategoryController@getCategoryBySlug');

/**
 * Categories
 */

Route::get('admin/categories/all', 'API\CategoryController@getCategories');
Route::post('admin/categories/store', 'API\CategoryController@store');
Route::post('admin/categories/update', 'API\CategoryController@update');
Route::delete('admin/categories/delete/{category}', 'API\CategoryController@destroy');

/**
 * Channels
 */

Route::get('admin/channels/byCategory/{categoryId}', 'API\ChannelController@getByCategory');
Route::get('admin/channels/all', 'API\ChannelController@getChannels');
Route::post('admin/channels/store', 'API\ChannelController@store');
Route::post('admin/channels/update', 'API\ChannelController@update');
Route::delete('admin/channels/delete/{channel}', 'API\ChannelController@destroy');

/**
 * User
 */
Route::post('user/sendcode', 'User\UserTelegramController@sendCode');
Route::post('user/login', 'User\UserTelegramController@login');
Route::get('user/loginstatus', 'User\UserTelegramController@loginStatus');
Route::get('user/logout', 'User\UserTelegramController@logout');
